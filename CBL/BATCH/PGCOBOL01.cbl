        IDENTIFICATION DIVISION.
        PROGRAM-ID. PGCOBOL01.
        ENVIRONMENT DIVISION.
        INPUT-OUTPUT SECTION.
        DATA DIVISION.
        WORKING-STORAGE SECTION.
        01 SCODER PIC 9(5).
        77 ws-dummy    PIC x.

        PROCEDURE DIVISION.
        START-PARA.
        DISPLAY "START PARA".
        MOVE 0 TO SCODER
        IF  ADDRESS OF SCODER = NULL
            DISPLAY "IF"
            MOVE 1 TO SCODER
            DISPLAY "SCODER VALUE: "SCODER. 
        ELSE
             DISPLAY "ELSE"
             MOVE 2 TO SCODER
             DISPLAY "SCODER VALUE: "SCODER.
        END-IF.    
        END-START-PARA.
        ACCEPT ws-dummy.
        EXIT.
